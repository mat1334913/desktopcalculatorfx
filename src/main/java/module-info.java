module com.devwider.fx.calculator {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.web;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires validatorfx;
    requires org.kordamp.ikonli.javafx;
    requires org.kordamp.bootstrapfx.core;
    requires eu.hansolo.tilesfx;

    exports com.matszym.calculator.service;
    opens com.matszym.calculator.service to javafx.fxml;
    exports com.matszym.calculator.service.operation;
    opens com.matszym.calculator.service.operation to javafx.fxml;
    exports com.matszym.calculator.controller;
    opens com.matszym.calculator.controller to javafx.fxml;
    exports com.matszym.calculator;
    opens com.matszym.calculator to javafx.fxml;
}